var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var nodemon = require('gulp-nodemon');
var plumber = require('gulp-plumber');


gulp.task('less', function () {
  return gulp.src('./public/stylesheets/style.less')
    .pipe(plumber())
    .pipe(less({}))
    .pipe(gulp.dest('./public/stylesheets'));
});

gulp.task('watch', function() {

  return gulp.watch('./public/stylesheets/**/*.less', ['less']);
})
// 
// gulp.task('watch', function(){
//   return gulp.watch('./public/javascript/**/*.js', ['dev']);
// })

gulp.task('dev', ['watch'], function(){

  return nodemon({
    script: 'bin/www',
    ext: 'html js'
  })
  .on('restart', function(){
    console.log('nodemon restarded');
  })
})
