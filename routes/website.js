var express = require('express');
var router = express.Router();


router.get('/', function(req, res) {
  res.render('website', {title: 'Less Template'});
});

module.exports = router;
